// Variables
const carrito = document.getElementById('carrito');
const cursos = document.getElementById('lista-cursos');
const listaCursos = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.getElementById('vaciar-carrito');


// Listeners
cargarEventListeners();

function comprarCurso(event) {
  //event.target.dataset.id
  var parentNode = event.srcElement.parentNode;
  var children = parentNode.childNodes;
  var imagen = parentNode.parentNode.getElementsByTagName('img')[0].src;
  var imagen = "<img  src =" + imagen + " class='imagen-curso u-full-width'>";
  var nombre = children[1].innerHTML;
  var precio = children[7].getElementsByClassName('u-pull-right')[0].innerHTML;

  var row = listaCursos.insertRow(0);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  var cell4 = row.insertCell(3);

  cell1.innerHTML = imagen;
  cell2.innerHTML = nombre;
  cell3.innerHTML = precio;
  cell4.innerHTML = "<button id =" + event.target.dataset.id + "  type='button'>borrar</button>";

  var producto = new Object();
  producto.imagen = imagen;
  producto.nombre = nombre;
  producto.precio = precio;
  var productoString = JSON.stringify(producto);

  localStorage.setItem(event.target.dataset.id, productoString);
}

function eliminarCurso(event) {
  if (listaCursos.rows.length != 0) {
    console.log(event.target.id);
    var parentNode = event.srcElement.parentNode.parentNode;
    console.log(parentNode);
    parentNode.remove();
    localStorage.removeItem(event.target.id);

    //localStorage.removeItem();

  }

}

function vaciarCarrito(event) {
  while (listaCursos.firstChild) {
    listaCursos.removeChild(listaCursos.firstChild);
  }
  if (validLocalStorage()) {
    localStorage.clear();
  }


}

function leerLocalStorage(event) {

  for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    var carrito = JSON.parse(localStorage.getItem(key));
    var row = listaCursos.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = carrito.imagen;
    cell2.innerHTML = carrito.nombre;
    cell3.innerHTML = carrito.precio;
    cell4.innerHTML = "<button id=" + key + "  type='button'>borrar</button>";


  }
}

function cargarEventListeners() {
  // Dispara cuando se presiona "Agregar Carrito"
  cursos.addEventListener('click', comprarCurso);

  // Cuando se elimina un curso del carrito
  carrito.addEventListener('click', eliminarCurso);

  // Al Vaciar el carrito
  vaciarCarritoBtn.addEventListener('click', vaciarCarrito);

  // Al cargar el documento, mostrar LocalStorage
  document.addEventListener('DOMContentLoaded', leerLocalStorage);

}
